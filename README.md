# Getting Started with Bubble Tea

## Available Scripts

In the project directory, you can run:

### `yarn install`

yarn install is used to install all dependencies for a project. This is most commonly used when you have just checked out code for a project, or when another developer on the project has added a new dependency that you need to pick up.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## The bubble tea

To choose your bubble tea click the validate button then you can:

- Choose your cup size
- Choose your cup color
- Write your name
- Choose your favorite taste
- Choose if you want tapioca

## Techno

ReactJS / SASS / JSX / React Spring
