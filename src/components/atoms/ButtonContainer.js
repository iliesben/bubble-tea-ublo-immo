const ButtonContainer = ({ icon, option, getOption }) => {

  return (
    <div className="button-container" onClick={() => getOption(option)}>
      <img src={icon} alt="icons" />
    </div>
  )
}

export default ButtonContainer;