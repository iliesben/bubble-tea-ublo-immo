import { useSpring, animated } from 'react-spring'

const Anim = ({ children }) => {

  const styles = useSpring({
    from: {
      x: -300,
      opacity: 0
    },
    to: {
      x: 0,
      opacity: 1
    },
    config: { duration: 750 }
  })

  return (
    <animated.div className="screen-container" style={styles}>
      {children}
    </animated.div>
  )
}

export default Anim;