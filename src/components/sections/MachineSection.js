import Cup from '../svg/Cup'
import Juice from '../svg/Juice'
import { useSpring } from 'react-spring'
import { useEffect } from 'react'

const MachineSection = ({ bubbleTea }) => {

  const [{ x }, api] = useSpring(() => ({
    config: {
      duration: 2000
    },
    x: 0
  }))

  useEffect(() => {
    api.start({
      from: { x: 0 },
      to: { x: 250 }
    })
  }, [bubbleTea.taste, api])

  return (
    <div className="machine-section-container">
      { bubbleTea.taste !== 'none' &&
        <div className="juice-container">
          <Juice
            taste={bubbleTea.taste}
            dJuice={
              x.to({
                range: [0, 75, 225, 250],
                output: [
                  "M0 0a0 5 0 0110 0v5a5 5 0 01-10 0V5z",
                  "M0 0a0 5 0 0110 0v185a5 5 0 01-10 0V20z",
                  "M0 5a5 5 0 0110 0v185a5 5 0 01-10 0V20z",
                  "M0 185a5 5 0 0110 0v5a5 5 0 01-10 0v-5z"
                ],
              })
            }
          />
        </div>
      }
      {
        bubbleTea.size !== null &&
        <div className={`cup-container ${bubbleTea.size && bubbleTea.size.toLowerCase()}`}>
          <Cup
            color={bubbleTea.color}
            text={bubbleTea.text}
            taste={bubbleTea.taste}
            tapioca={bubbleTea.tapioca}
            dTaste={
              x.to({
                range: [70, 250],
                output: [
                  "M23.0259 152H98.9062L98.7891 153H23.1328L23.0259 152Z",
                  "M8 13H115L98.7879 153H23.1313L8 13Z",
                ],
              })
            }
          >
          </Cup>
        </div>
      }
    </div >
  );
}

export default MachineSection;