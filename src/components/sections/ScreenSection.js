import Post from '../screenElements/Post'
import CupSize from '../screenElements/CupSize'
import CupColor from '../screenElements/CupColor'
import BubbleTaste from '../screenElements/BubbleTaste'
import CupText from '../screenElements/CupText'
import BubbleTapioca from '../screenElements/BubbleTapioca'

const ScreenSection = ({ changeScreen, getCupSize, getCupColor, getCupText, getBubbleTaste, getBubbleTapioca }) => {

  return (
    <div className="screen-section-container">
      { changeScreen === 0 && <Post title={"Click the button to "} caption={" choose your 🧋"} />}
      { changeScreen === 1 && <CupSize cupSize={getCupSize} />}
      { changeScreen === 2 && <CupColor cupColor={getCupColor} />}
      { changeScreen === 3 && <CupText cupText={getCupText} />}
      { changeScreen === 4 && <BubbleTaste bubbleTaste={getBubbleTaste} />}
      { changeScreen === 5 && <BubbleTapioca bubbleTapioca={getBubbleTapioca} />}
      { changeScreen === 6 && <Post title={"Enjoy your"} caption={" Bubble Tea 🧋"} />}
    </div>
  );
}

export default ScreenSection;