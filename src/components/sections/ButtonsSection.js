import CustomButton from '../atoms/ButtonContainer'
import Arrow from '../../icons/arrow.svg'
import Home from '../../icons/home.svg'
import Check from '../../icons/check.svg'

const ScreenSection = ({ getButtonOption }) => {

  return (
    <div className="buttons-section-container">
      <CustomButton icon={Arrow} option={"back"} getOption={getButtonOption} />
      <CustomButton icon={Home} option={"home"} getOption={getButtonOption} />
      <CustomButton icon={Check} option={"validate"} getOption={getButtonOption} />
    </div>
  );
}

export default ScreenSection;