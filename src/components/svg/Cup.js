import React from "react";
import { animated } from 'react-spring'

const BigCup = ({ color = "#C4C4C4", taste = "none", tapioca = "none", text, dTaste }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="124"
      height="154"
      fill="none"
      viewBox="0 0 124 154"
    >
      <animated.path
        fill={taste}
        d={dTaste}
      ></animated.path>
      {tapioca.length !== 'none' &&
        <>
          <circle cx="39.417" cy="116.417" r="5.417" fill={tapioca}></circle>
          <ellipse
            cx="91.333"
            cy="124.083"
            fill={tapioca}
            rx="6.667"
            ry="6.25"
          ></ellipse>
          <ellipse
            cx="71.75"
            cy="128.667"
            fill={tapioca}
            rx="7.083"
            ry="6.667"
          ></ellipse>
          <ellipse
            cx="31.083"
            cy="128.667"
            fill={tapioca}
            rx="7.083"
            ry="6.667"
          ></ellipse>
          <ellipse
            cx="47.583"
            cy="142.833"
            fill={tapioca}
            rx="7.083"
            ry="6.667"
          ></ellipse>
          <ellipse
            cx="88.833"
            cy="140.75"
            fill={tapioca}
            rx="5.833"
            ry="5.417"
          ></ellipse>
          <ellipse
            cx="49.833"
            cy="104.417"
            fill={tapioca}
            rx="5.833"
            ry="5.417"
          ></ellipse>
          <ellipse
            cx="64.25"
            cy="113.667"
            fill={tapioca}
            rx="4.583"
            ry="4.167"
          ></ellipse>
          <ellipse
            cx="28.583"
            cy="107.167"
            fill={tapioca}
            rx="4.583"
            ry="4.167"
          ></ellipse>
          <ellipse
            cx="60.083"
            cy="137"
            fill={tapioca}
            rx="4.583"
            ry="4.167"
          ></ellipse>
          <circle cx="73.834" cy="144.5" r="5" fill={tapioca}></circle>
          <circle cx="56.333" cy="122" r="4.167" fill={tapioca}></circle>
          <circle cx="48" cy="128.667" r="4.167" fill={tapioca}></circle>
          <circle cx="30.167" cy="144.167" r="4.167" fill={tapioca}></circle>
          <circle cx="73.167" cy="103.167" r="4.167" fill={tapioca}></circle>
          <ellipse
            cx="80.583"
            cy="114.167"
            fill={tapioca}
            rx="4.583"
            ry="4.167"
          ></ellipse>
          <circle cx="93.416" cy="110.75" r="3.75" fill={tapioca}></circle>
        </>
      } {tapioca !== 'none ' &&
        <>
          <circle cx="39.417" cy="116.417" r="5.417" fill={tapioca}></circle>
          <ellipse
            cx="91.333"
            cy="124.083"
            fill={tapioca}
            rx="6.667"
            ry="6.25"
          ></ellipse>
          <ellipse
            cx="71.75"
            cy="128.667"
            fill={tapioca}
            rx="7.083"
            ry="6.667"
          ></ellipse>
          <ellipse
            cx="31.083"
            cy="128.667"
            fill={tapioca}
            rx="7.083"
            ry="6.667"
          ></ellipse>
          <ellipse
            cx="47.583"
            cy="142.833"
            fill={tapioca}
            rx="7.083"
            ry="6.667"
          ></ellipse>
          <ellipse
            cx="88.833"
            cy="140.75"
            fill={tapioca}
            rx="5.833"
            ry="5.417"
          ></ellipse>
          <ellipse
            cx="49.833"
            cy="104.417"
            fill={tapioca}
            rx="5.833"
            ry="5.417"
          ></ellipse>
          <ellipse
            cx="64.25"
            cy="113.667"
            fill={tapioca}
            rx="4.583"
            ry="4.167"
          ></ellipse>
          <ellipse
            cx="28.583"
            cy="107.167"
            fill={tapioca}
            rx="4.583"
            ry="4.167"
          ></ellipse>
          <ellipse
            cx="60.083"
            cy="137"
            fill={tapioca}
            rx="4.583"
            ry="4.167"
          ></ellipse>
          <circle cx="73.834" cy="144.5" r="5" fill={tapioca}></circle>
          <circle cx="56.333" cy="122" r="4.167" fill={tapioca}></circle>
          <circle cx="48" cy="128.667" r="4.167" fill={tapioca}></circle>
          <circle cx="30.167" cy="144.167" r="4.167" fill={tapioca}></circle>
          <circle cx="73.167" cy="103.167" r="4.167" fill={tapioca}></circle>
          <ellipse
            cx="80.583"
            cy="114.167"
            fill={tapioca}
            rx="4.583"
            ry="4.167"
          ></ellipse>
          <circle cx="93.416" cy="110.75" r="3.75" fill={tapioca}></circle>
        </>
      }
      <path fill={color} d="M13 57h97l-5.243 39h-87.17L13 57z"></path>
      <path
        fill="#B0C7DB"
        fillOpacity="0.25"
        stroke="#B0C7DB"
        strokeWidth="2"
        d="M22.994 152.168L7.6 3.596h108.106L99.21 152.168H22.994z"
      ></path>
      <g clipPath="url(#clip0)">
        <text x="61" y="81" fill="black" textAnchor="middle" fontFamily="Cookie" fontSize="14">
          {text}
        </text>
      </g>
      <rect width="123.313" height="6.49" fill="#B0C7DB" rx="3.245"></rect>
      <defs>
        <clipPath id="clip0">
          <path
            fill="#fff"
            d="M0 0H90V39H0z"
            transform="translate(16.25 57)"
          ></path>
        </clipPath>
      </defs>
    </svg>
  )
}

export default BigCup;
