import React from "react";
import { animated } from 'react-spring'

const Juice = ({ taste, dJuice }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="10"
      height="195"
      fill="none"
      viewBox="0 0 10 195"
    >
      <animated.path
        fill={taste}
        d={dJuice}>
      </animated.path>
    </svg>
  );
}

export default Juice;
