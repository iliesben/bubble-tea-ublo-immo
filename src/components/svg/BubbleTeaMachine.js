import React from "react";

const BubbleTeaMachine = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="950"
      height="950"
      fill="none"
      viewBox="0 0 950 950"
    >
      <path
        fill="#8C8C8C"
        d="M0 672c0-5.523 4.477-10 10-10h930c5.523 0 10 4.477 10 10v39c0 5.523-4.477 10-10 10H10c-5.523 0-10-4.477-10-10v-39z"
      ></path>
      <rect
        width="288"
        height="62.889"
        x="99.883"
        y="662"
        fill="#8C8C8C"
        rx="10"
        transform="rotate(90 99.883 662)"
      ></rect>
      <rect
        width="288"
        height="62.889"
        x="905.607"
        y="662"
        fill="#8C8C8C"
        rx="10"
        transform="rotate(90 905.607 662)"
      ></rect>
      <rect
        width="52.318"
        height="23.781"
        x="247.891"
        y="638.587"
        fill="#131313"
        rx="4"
      ></rect>
      <rect
        width="52.318"
        height="23.781"
        x="647.413"
        y="638.587"
        fill="#131313"
        rx="4"
      ></rect>
      <path fill="#fff" d="M236 599.348H711.6220000000001V643.343H236z"></path>
      <ellipse
        cx="475"
        cy="223.012"
        fill="#272323"
        rx="123.662"
        ry="27.943"
      ></ellipse>
      <ellipse
        cx="472.622"
        cy="237.281"
        fill="#272323"
        rx="105.826"
        ry="30.321"
      ></ellipse>
      <path fill="#8B8B8B" d="M269.293 177.234H278.805V187.935H269.293z"></path>
      <path
        fill="#8B8B8B"
        d="M660.492 177.234H670.0039999999999V187.935H660.492z"
      ></path>
      <circle cx="665.249" cy="177.234" r="14.269" fill="#272323"></circle>
      <rect
        width="152.199"
        height="130.796"
        x="398.9"
        y="223.607"
        fill="#272323"
        rx="8"
      ></rect>
      <g filter="url(#filter0_d)">
        <path
          fill="#8B8687"
          d="M259.781 189.124H686.652V621.9399999999999H259.781z"
        ></path>
      </g>
      <g filter="url(#filter1_i)">
        <path
          fill="#333030"
          d="M287.129 316.139c0-11.045 8.955-20 20-20h333.364c11.045 0 20 8.955 20 20v277.478c0 11.046-8.955 20-20 20H307.129c-11.045 0-20-8.954-20-20V316.139z"
        ></path>
      </g>
      <rect
        width="404.279"
        height="23.781"
        x="271.672"
        y="586.269"
        fill="#131313"
        rx="10"
      ></rect>
      <ellipse
        cx="473.811"
        cy="588.052"
        fill="#131313"
        rx="164.09"
        ry="6.54"
      ></ellipse>
      <path
        fill="#CACACA"
        d="M236 599.348H711.6220000000001V643.343H236z"
      ></path>
      <g fill="#3E3E3E" filter="url(#filter2_d)">
        <ellipse cx="473.216" cy="272.465" rx="125.445" ry="27.943"></ellipse>
        <ellipse cx="472.622" cy="290.301" rx="93.935" ry="26.754"></ellipse>
        <rect
          width="118.905"
          height="130.796"
          x="413.169"
          y="257.602"
          rx="8"
        ></rect>
        <path d="M420.898 309.92h-7.135l-2.378-.594-30.915-19.025-1.189-.595h-.595v-.594l-.594-1.189 7.729-14.863h171.818l8.918 4.756v11.89l-26.159 17.836-8.324 2.378H420.898z"></path>
      </g>
      <g filter="url(#filter3_d)">
        <path
          fill="#CACACA"
          d="M236 14c0-5.523 4.477-10 10-10h458c5.523 0 10 4.477 10 10v160c0 55.228-44.772 100-100 100H336c-55.228 0-100-44.772-100-100V14z"
        ></path>
      </g>
      <path
        fill="#F1FEFF"
        d="M270 38c0-5.523 4.477-10 10-10h390c5.523 0 10 4.477 10 10v155c0 16.569-13.431 30-30 30H300c-16.569 0-30-13.431-30-30V38z"
      ></path>
      <circle
        cx="549"
        cy="248"
        r="14"
        fill="#282737"
        transform="rotate(-90 549 248)"
      ></circle>
      <circle
        cx="401"
        cy="248"
        r="14"
        fill="#282737"
        transform="rotate(-90 401 248)"
      ></circle>
      <circle
        cx="475"
        cy="248"
        r="14"
        fill="#282737"
        transform="rotate(-90 475 248)"
      ></circle>
      <defs>
        <filter
          id="filter0_d"
          width="434.871"
          height="440.816"
          x="255.781"
          y="189.124"
          colorInterpolationFilters="sRGB"
          filterUnits="userSpaceOnUse"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          ></feColorMatrix>
          <feOffset dy="4"></feOffset>
          <feGaussianBlur stdDeviation="2"></feGaussianBlur>
          <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"></feColorMatrix>
          <feBlend
            in2="BackgroundImageFix"
            result="effect1_dropShadow"
          ></feBlend>
          <feBlend
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          ></feBlend>
        </filter>
        <filter
          id="filter1_i"
          width="373.363"
          height="321.478"
          x="287.129"
          y="296.139"
          colorInterpolationFilters="sRGB"
          filterUnits="userSpaceOnUse"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
          <feBlend
            in="SourceGraphic"
            in2="BackgroundImageFix"
            result="shape"
          ></feBlend>
          <feColorMatrix
            in="SourceAlpha"
            result="hardAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          ></feColorMatrix>
          <feOffset dy="4"></feOffset>
          <feGaussianBlur stdDeviation="6"></feGaussianBlur>
          <feComposite
            in2="hardAlpha"
            k2="-1"
            k3="1"
            operator="arithmetic"
          ></feComposite>
          <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"></feColorMatrix>
          <feBlend in2="shape" result="effect1_innerShadow"></feBlend>
        </filter>
        <filter
          id="filter2_d"
          width="314.891"
          height="207.876"
          x="315.771"
          y="216.522"
          colorInterpolationFilters="sRGB"
          filterUnits="userSpaceOnUse"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          ></feColorMatrix>
          <feOffset dy="4"></feOffset>
          <feGaussianBlur stdDeviation="16"></feGaussianBlur>
          <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"></feColorMatrix>
          <feBlend
            in2="BackgroundImageFix"
            result="effect1_dropShadow"
          ></feBlend>
          <feBlend
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          ></feBlend>
        </filter>
        <filter
          id="filter3_d"
          width="494"
          height="286"
          x="228"
          y="0"
          colorInterpolationFilters="sRGB"
          filterUnits="userSpaceOnUse"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
          <feColorMatrix
            in="SourceAlpha"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          ></feColorMatrix>
          <feOffset dy="4"></feOffset>
          <feGaussianBlur stdDeviation="4"></feGaussianBlur>
          <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"></feColorMatrix>
          <feBlend
            in2="BackgroundImageFix"
            result="effect1_dropShadow"
          ></feBlend>
          <feBlend
            in="SourceGraphic"
            in2="effect1_dropShadow"
            result="shape"
          ></feBlend>
        </filter>
      </defs>
    </svg>
  );
}

export default BubbleTeaMachine;
