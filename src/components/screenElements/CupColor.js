import Anim from '../utils/anim'

const CupColor = ({ cupColor }) => {

  const cupColors = ["#FED440", "#5784BA", "#0FAC71", "#FE94B4", "#C23028", "#692FA0"]

  return (
    <Anim>
      <h2>Choose your cup color</h2>
      <div className="screen-icons-main-container">
        {
          cupColors.map((color, index) => (
            <div key={index} className="cup-color-container" style={{ 'backgroundColor': color }} onClick={() => cupColor(color)}></div>
          ))
        }
      </div>
    </Anim>
  );
}

export default CupColor;