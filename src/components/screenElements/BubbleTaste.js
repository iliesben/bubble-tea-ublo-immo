import Anim from '../utils/anim'

const BubbleTaste = ({ bubbleTaste }) => {

  const bubbleTastes = [
    {
      name: 'Watermelon',
      color: "#a7596a",
    },
    {
      name: 'Apple',
      color: "#7c9a67",
    },
    {
      name: 'Pomegranate',
      color: "#7b5c67",
    },
    {
      name: 'Passion',
      color: "#5F4C4C",
    },
    {
      name: 'Mango',
      color: "#ac965b",
    },
    {
      name: 'Peach',
      color: "#ad928a",
    }
  ]

  return (
    <Anim>
      <h2>Choose your bubble taste</h2>
      <div className="screen-icons-main-container">
        {
          bubbleTastes.map((taste, index) => (
            <div key={index} className="bubble-taste-container" onClick={() => bubbleTaste(taste.color)}>
              <p> {taste.name} </p>
            </div>
          ))
        }
      </div>
    </Anim>
  );
}

export default BubbleTaste;