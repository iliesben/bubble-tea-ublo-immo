import Anim from '../utils/anim'

const CupText = ({ cupText }) => {

  const getValue = (value) => cupText(value)

  return (
    <Anim>
      <h2>Write your name :</h2>
      <div className="cup-text-container">
        <input type="text" name="text" maxLength="15" autoComplete="off" onChange={(e) => getValue(e.target.value)} />
      </div>
    </Anim>
  );
}

export default CupText;