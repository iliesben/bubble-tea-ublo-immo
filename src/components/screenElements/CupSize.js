import BigCup from '../../icons/big-cup.svg';
import MediumCup from '../../icons/medium-cup.svg';
import SmallCup from '../../icons/small-cup.svg';
import Anim from '../utils/anim'

const CupSize = ({ visibility, cupSize }) => {

  const cupSizes = [
    {
      name: 'Large',
      icon: BigCup,
    },
    {
      name: 'Medium',
      icon: MediumCup,
    },
    {
      name: 'Small',
      icon: SmallCup,
    }
  ]

  return (
    <Anim>
      <h2>Choose your cup size</h2>
      <div className="screen-cup-size-icons-main-container">
        {
          cupSizes.map((cup, index) => (
            <div key={index} className="cup-size-icons-container" onClick={() => cupSize(cup.name)}>
              <img src={cup.icon} alt={`${cup.name} Cup`} />
              <p> {cup.name} </p>
            </div>
          ))
        }
      </div>
    </Anim>
  );
}

export default CupSize;