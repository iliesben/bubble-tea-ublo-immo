import Anim from '../utils/anim'

const BubbleTapioca = ({ bubbleTapioca }) => {

  const bubbleTapiocas = [
    {
      name: 'Black Tapioca',
      color: "#000",
    },
    {
      name: 'none',
      color: "none",
    },
    {
      name: 'White Tapioca',
      color: "#FFF",
    }
  ]

  return (
    <Anim>
      <h2>Choose your bubble tapioca</h2>
      <div className="screen-icons-main-container">
        {
          bubbleTapiocas.map((tapioca, index) => (
            <div key={index} className="bubble-tapioca-container" onClick={() => bubbleTapioca(tapioca.color)}>
              <p> {tapioca.name} </p>
            </div>
          ))
        }
      </div>
    </Anim>
  );
}

export default BubbleTapioca;