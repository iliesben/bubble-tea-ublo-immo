import { useSpring, animated } from 'react-spring'

const Post = ({ title, caption }) => {

  const styles = useSpring({
    to: { opacity: 1 },
    from: { opacity: 0 },
    loop: { reverse: true },
    reset: true,
    delay: 500,
    config: { duration: 1500 }
  })

  return (
    <div className="screen-container">
      <div className="post-container">
        <animated.div style={styles}>
          <h2>{title}<span> {caption} </span> </h2>
        </animated.div>
      </div>
    </div>
  )
}

export default Post;