import { useState } from 'react'
import BubbleTeaMachine from './svg/BubbleTeaMachine'
import ButtonsSection from './sections/ButtonsSection'
import ScreenSection from './sections/ScreenSection'
import MachineSection from './sections/MachineSection'

const Sections = () => {

  const cup = {
    size: null,
    color: "#B0C7DB",
    text: "",
    taste: "none",
    tapioca: "none"
  }

  const [changeScreenState, setChangeScreenState] = useState(0)
  const [cupState, setCupState] = useState(cup)

  const screenForward = () => {
    if ((changeScreenState === 1 && cupState.size === null) || (changeScreenState === 4 && cupState.taste === 'none')) return
    setChangeScreenState(changeScreenState + 1)
  }

  const screenBack = () => {
    if (changeScreenState === 2) setCupState({ ...cupState, size: cup.size, color: cup.color })
    else if (changeScreenState === 3) setCupState({ ...cupState, color: cup.color, text: cup.text })
    else if (changeScreenState === 4) setCupState({ ...cupState, text: cup.text, taste: cup.taste })
    else if (changeScreenState === 5) setCupState({ ...cupState, taste: cup.taste, tapioca: cup.tapioca })
    else if (changeScreenState === 6) setCupState({ ...cupState, tapioca: cup.tapioca })
    setChangeScreenState(changeScreenState - 1)
  }

  const buttonOption = (option) => {
    if (option === 'validate' && changeScreenState < 6) screenForward()
    else if (option === 'back' && changeScreenState > 0) screenBack()
    else if (option === 'home' && changeScreenState !== 0) {
      setCupState(cup)
      setChangeScreenState(0)
    }
  }

  const cupSize = (size) => {
    if (size !== null && size !== cupState.size) setCupState({ ...cupState, size })
  }

  const cupColor = (color) => {
    if (color !== null && color !== cupState.color) setCupState({ ...cupState, color })
  }

  const bubbleTaste = (taste) => {
    if (taste !== null && taste !== cupState.taste) setCupState({ ...cupState, taste })
  }

  const bubbleTapioca = (tapioca) => {
    if (tapioca !== null && tapioca !== cupState.tapioca) setCupState({ ...cupState, tapioca })
  }

  const cupText = (text) => {
    if (text !== null && text !== cupState.text) setCupState({ ...cupState, text })
  }

  return (
    <div className="App">
      <div className="main-container">
        <div className="bubble-main-container">
          <BubbleTeaMachine />
          <ButtonsSection getButtonOption={buttonOption} />
          <ScreenSection changeScreen={changeScreenState} getCupSize={cupSize} getCupColor={cupColor} getCupText={cupText} getBubbleTaste={bubbleTaste} getBubbleTapioca={bubbleTapioca} />
          <MachineSection bubbleTea={cupState} />
        </div>
      </div>
    </div>
  );
}

export default Sections;